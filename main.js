/* 
    there are other js files in assets/comps/.
    each of those files defines custom element used in this webpage. 
*/

(function () {
    document.querySelector('notification-panel').addEventListener('hide-notification', function (e) {
        /* when user click got it in notification panel,
            move wrapper element below with the same speed and distance */
        let transitionDistance = e.target.querySelector('section').offsetHeight
        let lastKnownScrollPosition = window.scrollY;
        let viewHeight = window.innerHeight;
        let documentHeight = document.body.offsetHeight;

        /* animate only when scroll position is not too far bottom to prevent flashing */
        if (documentHeight - viewHeight - lastKnownScrollPosition > transitionDistance) {
            const wrapper = document.querySelector('.wrapper')
            wrapper.style.transition = `transform ${e.detail.transitionTime / 1000}s`
            wrapper.style.transform = `translateY(-${transitionDistance}px)`
    
            /* after transition finished, reset wrapper position */
            setTimeout(() => {
                wrapper.style.transform = ""
                wrapper.style.transition = ""
            }, e.detail.transitionTime)
        }
    }, false);
})()

