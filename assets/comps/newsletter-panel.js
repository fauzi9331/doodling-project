(function () {
    const TRANSITION_TIME = 500;

    function shouldShow() {
        /* determine wether newsletter has been recently closed or not (within 10min),
            if not, return true
         */
        const newsletterLastClosed = window.localStorage.getItem("newsletterLastClosed")
        if (!newsletterLastClosed || ((new Date() - new Date(newsletterLastClosed)) > (1000 * 60 * 10))) {
            return true
        } else {
            return false
        }
    }

    class NewsLetterPanel extends HTMLElement {
        get hidden() {
            return this.hasAttribute('hidden');
        }

        set hidden(val) {
            if (val) {
                let div = this.querySelector("div")
                if (div) {
                    /* if element already rendered, add animation */
                    div.style.transition = `transform ${TRANSITION_TIME / 1000}s`
                    div.style.transform = "translateY(100%)"

                    /* wait until transition finished, then set hidden attribute */
                    setTimeout(() => {
                        this.setAttribute('hidden', '');
                    }, TRANSITION_TIME)
                } else {
                    /* element has not been rendered, immediately hide */
                    this.setAttribute('hidden', '');
                }
            } else {
                div.style.transform = "translateY(0%)"
                this.removeAttribute('hidden');
            }
        }

        constructor() {
            super();
            this.hidden = true
        }

        setupScrollListener(callback) {
            /* setting up scroll listener, callback function will be executed if
                scroll position at least a third of document's height*/
            let lastKnownScrollPosition = window.scrollY;
            let viewHeight = window.innerHeight;
            let documentHeight = document.body.offsetHeight;
            let ticking = false;
            let thisElement = document.querySelector('newsletter-panel')
            let isHidden = thisElement.hidden

            /* call once if condition met */
            if (isHidden && lastKnownScrollPosition > (documentHeight - viewHeight) / 3) {
                callback(thisElement)
            }

            document.addEventListener('scroll', function (e) {
                lastKnownScrollPosition = window.scrollY;

                if (!ticking) {
                    isHidden = thisElement.hidden
                    window.requestAnimationFrame(function () {
                        if (isHidden && lastKnownScrollPosition > (documentHeight - viewHeight) / 3) {
                            callback(thisElement)
                        }
                        ticking = false;
                    });
                    ticking = true;
                }
            });
        }

        show(thisElement) {
            if (shouldShow()) {
                thisElement.removeAttribute('hidden');
                /* wait to ensure hidden attribute fully removed, if not animation won't happen */
                setTimeout(() => {
                    thisElement.querySelector('div').style.transform = "translateY(0%)"
                }, 100)
            }
        }
        connectedCallback() {
            this.render();
        }

        render() {
            this.innerHTML = `
            <div>
                <button><i class="fas fa-times"></i></button>
                <h1>Get latest updates in web technologies</h1>
                <p>I write articles related to web technologies, such as design trends, development
                    tools, UI/UX case studies and reviews, and more. Sign up to my newsletter to get
                    them all.<p>
                <form>
                    <input type="email" placeholder="Email address">
                    <input type="submit" value="Count Me In!">
                <form>
            </div>
            `;

            this.setupScrollListener(this.show)

            this.querySelector("button").addEventListener('click', e => {
                window.localStorage.setItem("newsletterLastClosed", new Date())
                this.hidden = true
            })

            this.querySelector("input[type='submit']").addEventListener('click', e => {
                e.preventDefault()
            })

            let div = this.querySelector("div")
            div.style.transform = "translateY(100%)"
            div.style.transition = `transform ${TRANSITION_TIME / 1000}s`
        }
    }
    customElements.define('newsletter-panel', NewsLetterPanel)
})()