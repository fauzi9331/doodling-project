(function () {
    class HighlightsPanel extends HTMLElement {
        constructor() {
            super();
            this.tiles = [
                {
                    title: "Consult",
                    description: `Co-create, design thinking; strengthen infrastructure resist granular.
                        Revolution circular, movements or framework social impact low-hanging fruit.
                        Save the world compelling revolutionary progress.`,
                    iconClass: "fas fa-comments",
                },
                {
                    title: "Design",
                    description: `Policymaker collaborates collective impact humanitarian shared value
                        vocabulary inspire issue outcomes agile. Overcome injustice deep dive agile 
                        issue outcomes vibrant boots on the ground sustainable.`,
                    iconClass: "fas fa-paint-brush",
                },
                {
                    title: "Develop",
                    description: `Revolutionary circular, movements a or impact framework social impact low-
                        hanging. Save the compelling revolutionary inspire progress. Collective
                        impacts and challenges for opportunities of thought provoking.`,
                    iconClass: "fas fa-cubes",
                },
                {
                    title: "Marketing",
                    description: `Peaceful; vibrant paradigm, collaborative cities. Shared vocabulary agile,
                        replicable, effective altruism youth. Mobilize commitment to overcome
                        injustice resilient, uplift social transparent effective.`,
                    iconClass: "fas fa-bullhorn",
                },
                {
                    title: "Manage",
                    description: `Change-makers innovation or shared unit of analysis. Overcome injustice
                        outcomes strategize vibrant boots on the ground sustainable. Optimism,
                        effective altruism invest optimism corporate social.`,
                    iconClass: "fas fa-tasks",
                },
                {
                    title: "Evolve",
                    description: `Activate catalyze and impact contextualize humanitarian. Unit of analysis
                        overcome injustice storytelling altruism. Thought leadership mass
                        incarceration. Outcomes big data, fairness, social game-changer.`,
                    iconClass: "fas fa-chart-line",
                },
            ]
        }

        connectedCallback() {
            this.render();
        }

        render() {
            this.innerHTML = `
            <section class="container">
                <div class="content">
                    <div class="header">
                        <h1>How Can I Help You?</h1>
                        <p>Our work then targeted, best practices outcomes social innovation synergy.
                        Venture philanthropy, revolutionary inclusive policymaker relief. User-centered
                        program areas scale.</p>
                    </div> 
                    <div class="tiles">
                    ${
                        this.tiles.map( tile => {
                            return `<highlights-panel-tile 
                                title="${tile.title}"
                                description="${tile.description}"
                                icon-class="${tile.iconClass}"
                            ></highlights-panel-tile>`
                        }).join('')
                    }
                    </div>
                </div>
            </section>
            `;
        }
    }

    class Tile extends HTMLElement {
        get title() {
            return this.getAttribute('title');
        }
        get description() {
            return this.getAttribute('description');
        }
        get iconClass() {
            return this.getAttribute('icon-class');
        }
        
        constructor() {
            super();
        }

        connectedCallback() {
            this.render();
        }

        render() {
            this.innerHTML = `
                <div class="tile">
                    <div>
                        <h3>${this.title}</h3>
                        <i class="${this.iconClass}"></i>
                    </div>
                    <p>${this.description}</p>
                </div>
            `;
        }
    }
    
    customElements.define('highlights-panel', HighlightsPanel);
    customElements.define('highlights-panel-tile', Tile);
})()