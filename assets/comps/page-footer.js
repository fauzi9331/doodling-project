(function () {
    class PageFooter extends HTMLElement {
        constructor() {
            super();
        }

        connectedCallback() {
            this.render()
        }

        render() {
            this.innerHTML = `
            <footer class="container">
                <div class="content">© 2021 Muhammad Iqbal Fauzi. All rights reserved.
                </div>
            </footer>
            `;
        }
    }

    customElements.define('page-footer', PageFooter);
})()