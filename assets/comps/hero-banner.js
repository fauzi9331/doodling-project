(function () {
    class HeroBanner extends HTMLElement {
        constructor() {
            super();
        }

        connectedCallback() {
            this.render()
        }

        render() {
            this.innerHTML = `
            <section class="container">
                <div class="page-header"><img src="assets/images/y-logo-white.png" width="50px" height="50px"></div> 
                <div class="hero-shot content">
                    <h1>Hello! I'm Muhammad Iqbal Fauzi</h1>
                    <h2>Consult, Design, and Develop Websites</h2>
                    <p>Have something great in mind? Feel free to contact me.<br>
                        I'll help you to make it happen.</p>
                    <button><p>Let's Make Contact</p></button>
                </div>
            </section>
            `;
        }
    }

    customElements.define('hero-banner', HeroBanner);
})()