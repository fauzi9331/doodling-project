(function () {
    const TRANSITION_TIME = 500;

    class NotificationPanel extends HTMLElement {
        get hidden() {
            return this.hasAttribute('hidden');
        }

        set hidden(val) {
            if (val) {
                this.dispatchEvent(this.event);

                /* start transition */
                let section = this.querySelector("section")
                section.style.transition = `transform ${TRANSITION_TIME/1000}s`
                section.style.transform = "translateY(-100%)"

                /* wait until transition finished, then set hidden attribute */
                setTimeout(() => {
                    this.setAttribute('hidden', '');
                }, TRANSITION_TIME)
            } else {
                this.removeAttribute('hidden');
            }
        }

        constructor() {
            super();
        }

        connectedCallback() {
            this.render();
        }

        render() {
            this.innerHTML = `
            <section class="container">
                <div class="content">
                    <p>By accessing and using this website, you acknowledge that you have read and
                        understand our <a href="#">Cookie&#160;Policy</a>, <a href="#">Privacy&#160;Policy</a>, and our <a href="#">Terms&#160;of&#160;Service</a>.</p>
                    <button><p>Got&#160;It</p></button>
                </div>
            <section>
            `;

            this.querySelector("button").addEventListener('click', e => { this.hidden = true })

            /* create event to send transition time when user click Got it, 
                this is for synchronization of movement */
            this.event = new CustomEvent('hide-notification', { detail: { 
                transitionTime: TRANSITION_TIME, 
            } });
        }
    }
    customElements.define('notification-panel', NotificationPanel)
})()